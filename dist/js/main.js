(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {

    "use strict";

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variables rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once

    var pluginName = "IliaWeatherWidget",
        defaults = {
        propertyName: "value"
    };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        //this.settings = $.extend( {}, defaults, options );
        this.settings = $.extend({
            'city': 'city',
            'units': 'metric',
            'lang': 'en'
        }, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.weatherImages = {
            'Clouds': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/clouds.jpg',
            'Rain': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/rain.jpg',
            'Snow': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/snow.jpg',
            'Mist': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/mist.jpg',
            'Clear': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/clear.jpg'
        }, this.unitsList = {
            metric: "\u2103",
            imperial: "\u2109",
            default: "\u212A"
        }, this.init();
    }
    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        init: function init() {
            var _this = this;

            this.getData(this.settings.city, this.settings.units, this.settings.lang, function (weatherData) {
                _this.displayLayout(_this.element);
                _this.setData(_this.weatherImages, _this.settings.units, weatherData);
                _this.initAutocomplete();
            });
        },

        getData: function getData(city, units, lang, callback) {
            $.ajax({
                dataType: "json",
                url: "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=" + units + "&lang=" + lang + "&appid=629dbe4bc64c26fd15de637072a99882",
                async: true,
                success: function success(data) {
                    callback({
                        city: city,
                        country: data.sys.country,
                        temp: data.main.temp,
                        weather: data.weather[0].description,
                        weatherCond: data.weather[0].main
                    });
                }
            });
        },

        displayLayout: function displayLayout(element) {

            //Выстраиваем DOM и вставляем его в HTML
            $(element).addClass('ww');
            $('<div>', {
                'class': 'input-wrapper',
                html: $('<label/>', {
                    for: 'city',
                    text: 'Write the city: '
                }).add($('<input/>', {
                    type: 'text',
                    id: 'city'
                }))
            }).add($('<div>', {
                'class': 'ww-background',
                html: $('<div>', {
                    'class': 'ww-wrapper',
                    html: $('<div>', {
                        'class': 'ww-other',
                        html: $('<p>', {
                            'class': 'ww-other-addition'
                        }).add($('<p>', {
                            'class': 'ww-other-location'
                        }))
                    }).add($('<div>', {
                        'class': 'ww-temperature',
                        html: $('<span>', {
                            'class': 'ww-temperature-value'
                        }).add($('<span>', {
                            'class': 'ww-temperature-unit'
                        }))
                    }))
                })
            })).appendTo(element);
        },

        setData: function setData(images, units, weatherData) {
            var imgUrl = images[weatherData.weatherCond];
            var unit = this.unitsList[units];

            $('.ww-background').css('background-image', 'url("' + imgUrl + '")');
            $('.ww-other-addition').text(weatherData.weather);
            $('.ww-other-location').text(weatherData.country + ', ' + weatherData.city);
            $('.ww-temperature-value').text(weatherData.temp.toFixed());
            $('.ww-temperature-unit').text(unit);
        },

        initAutocomplete: function initAutocomplete() {
            var _this2 = this;

            var $city = $('#city');

            $city.autocomplete({
                source: function source(request, response) {
                    jQuery.getJSON("http://gd.geobytes.com/AutoCompleteCity?callback=?&template=<geobytes%20city>&q=" + $city.val(), function (data) {
                        response(data);
                    });
                }
            });

            $city.on("autocompleteselect", function (event, ui) {
                var _settings = _this2.settings,
                    units = _settings.units,
                    lang = _settings.lang;

                _this2.getData(ui.item.value, units, lang, function (weatherData) {
                    _this2.setData(_this2.weatherImages, units, weatherData);
                });
            });
        }

    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvbWFpbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUE7QUFDQTtBQUNBLENBQUMsQ0FBRSxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTJDOztBQUUxQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBQ0EsUUFBSSxhQUFhLG1CQUFqQjtBQUFBLFFBQ0ksV0FBVztBQUNQLHNCQUFjO0FBRFAsS0FEZjs7QUFLQTtBQUNBLGFBQVMsTUFBVCxDQUFrQixPQUFsQixFQUEyQixPQUEzQixFQUFxQztBQUNqQyxhQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUssUUFBTCxHQUFnQixFQUFFLE1BQUYsQ0FBVTtBQUN0QixvQkFBUSxNQURjO0FBRXRCLHFCQUFVLFFBRlk7QUFHdEIsb0JBQVE7QUFIYyxTQUFWLEVBSWIsT0FKYSxDQUFoQjtBQUtBLGFBQUssU0FBTCxHQUFpQixRQUFqQjtBQUNBLGFBQUssS0FBTCxHQUFhLFVBQWI7O0FBRUEsYUFBSyxhQUFMLEdBQXFCO0FBQ2pCLHNCQUFVLHVFQURPO0FBRWpCLG9CQUFRLHFFQUZTO0FBR2pCLG9CQUFRLHFFQUhTO0FBSWpCLG9CQUFRLHFFQUpTO0FBS2pCLHFCQUFTO0FBTFEsU0FBckIsRUFPQSxLQUFLLFNBQUwsR0FBaUI7QUFDYixvQkFBUSxRQURLO0FBRWIsc0JBQVUsUUFGRztBQUdiLHFCQUFTO0FBSEksU0FQakIsRUFhQSxLQUFLLElBQUwsRUFiQTtBQWNIO0FBQ0Q7QUFDQSxNQUFFLE1BQUYsQ0FBVSxPQUFPLFNBQWpCLEVBQTRCOztBQUV4QixjQUFNLGdCQUFXO0FBQUE7O0FBQ2IsaUJBQUssT0FBTCxDQUFhLEtBQUssUUFBTCxDQUFjLElBQTNCLEVBQWlDLEtBQUssUUFBTCxDQUFjLEtBQS9DLEVBQXNELEtBQUssUUFBTCxDQUFjLElBQXBFLEVBQTBFLFVBQUMsV0FBRCxFQUFpQjtBQUN2RixzQkFBSyxhQUFMLENBQW1CLE1BQUssT0FBeEI7QUFDQSxzQkFBSyxPQUFMLENBQWEsTUFBSyxhQUFsQixFQUFpQyxNQUFLLFFBQUwsQ0FBYyxLQUEvQyxFQUFzRCxXQUF0RDtBQUNBLHNCQUFLLGdCQUFMO0FBQ0gsYUFKRDtBQUtILFNBUnVCOztBQVV4QixpQkFBUyxpQkFBUyxJQUFULEVBQWUsS0FBZixFQUFzQixJQUF0QixFQUE0QixRQUE1QixFQUFzQztBQUMzQyxjQUFFLElBQUYsQ0FBTztBQUNILDBCQUFVLE1BRFA7QUFFSCxxQkFBSyxzREFBcUQsSUFBckQsR0FBMkQsU0FBM0QsR0FBc0UsS0FBdEUsR0FBNkUsUUFBN0UsR0FBdUYsSUFBdkYsR0FBNkYseUNBRi9GO0FBR0gsdUJBQU8sSUFISjtBQUlILHlCQUFTLGlCQUFDLElBQUQsRUFBVTtBQUNmLDZCQUFTO0FBQ0wsOEJBQU0sSUFERDtBQUVMLGlDQUFTLEtBQUssR0FBTCxDQUFTLE9BRmI7QUFHTCw4QkFBTSxLQUFLLElBQUwsQ0FBVSxJQUhYO0FBSUwsaUNBQVMsS0FBSyxPQUFMLENBQWEsQ0FBYixFQUFnQixXQUpwQjtBQUtMLHFDQUFhLEtBQUssT0FBTCxDQUFhLENBQWIsRUFBZ0I7QUFMeEIscUJBQVQ7QUFPSDtBQVpFLGFBQVA7QUFjSCxTQXpCdUI7O0FBMkJ4Qix1QkFBZSx1QkFBUyxPQUFULEVBQWtCOztBQUU3QjtBQUNBLGNBQUUsT0FBRixFQUFXLFFBQVgsQ0FBb0IsSUFBcEI7QUFDQSxjQUFFLE9BQUYsRUFBVztBQUNQLHlCQUFTLGVBREY7QUFFUCxzQkFBTSxFQUFFLFVBQUYsRUFBYztBQUNoQix5QkFBSyxNQURXO0FBRWhCLDBCQUFNO0FBRlUsaUJBQWQsRUFHSCxHQUhHLENBSUYsRUFBRSxVQUFGLEVBQWM7QUFDViwwQkFBTSxNQURJO0FBRVYsd0JBQUk7QUFGTSxpQkFBZCxDQUpFO0FBRkMsYUFBWCxFQVdHLEdBWEgsQ0FZSSxFQUFFLE9BQUYsRUFBVTtBQUNOLHlCQUFTLGVBREg7QUFFTixzQkFBTSxFQUFFLE9BQUYsRUFBVztBQUNiLDZCQUFTLFlBREk7QUFFYiwwQkFBTSxFQUFFLE9BQUYsRUFBVztBQUNiLGlDQUFTLFVBREk7QUFFYiw4QkFBTSxFQUFFLEtBQUYsRUFBUztBQUNYLHFDQUFTO0FBREUseUJBQVQsRUFFSCxHQUZHLENBR0YsRUFBRSxLQUFGLEVBQVM7QUFDTCxxQ0FBUztBQURKLHlCQUFULENBSEU7QUFGTyxxQkFBWCxFQVNILEdBVEcsQ0FVRixFQUFFLE9BQUYsRUFBVztBQUNQLGlDQUFTLGdCQURGO0FBRVAsOEJBQU0sRUFBRSxRQUFGLEVBQVk7QUFDZCxxQ0FBUztBQURLLHlCQUFaLEVBRUgsR0FGRyxDQUdGLEVBQUUsUUFBRixFQUFZO0FBQ1IscUNBQVM7QUFERCx5QkFBWixDQUhFO0FBRkMscUJBQVgsQ0FWRTtBQUZPLGlCQUFYO0FBRkEsYUFBVixDQVpKLEVBdUNFLFFBdkNGLENBdUNXLE9BdkNYO0FBeUNILFNBeEV1Qjs7QUEwRXhCLGlCQUFTLGlCQUFTLE1BQVQsRUFBaUIsS0FBakIsRUFBd0IsV0FBeEIsRUFBcUM7QUFDMUMsZ0JBQU0sU0FBUyxPQUFPLFlBQVksV0FBbkIsQ0FBZjtBQUNBLGdCQUFNLE9BQU8sS0FBSyxTQUFMLENBQWUsS0FBZixDQUFiOztBQUVBLGNBQUUsZ0JBQUYsRUFBb0IsR0FBcEIsQ0FBd0Isa0JBQXhCLEVBQTRDLFVBQVMsTUFBVCxHQUFpQixJQUE3RDtBQUNBLGNBQUUsb0JBQUYsRUFBd0IsSUFBeEIsQ0FBNkIsWUFBWSxPQUF6QztBQUNBLGNBQUUsb0JBQUYsRUFBd0IsSUFBeEIsQ0FBNkIsWUFBWSxPQUFaLEdBQXNCLElBQXRCLEdBQTZCLFlBQVksSUFBdEU7QUFDQSxjQUFFLHVCQUFGLEVBQTJCLElBQTNCLENBQWdDLFlBQVksSUFBWixDQUFpQixPQUFqQixFQUFoQztBQUNBLGNBQUUsc0JBQUYsRUFBMEIsSUFBMUIsQ0FBK0IsSUFBL0I7QUFDSCxTQW5GdUI7O0FBcUZ4QiwwQkFBa0IsNEJBQVc7QUFBQTs7QUFDM0IsZ0JBQU0sUUFBUSxFQUFFLE9BQUYsQ0FBZDs7QUFFQSxrQkFBTSxZQUFOLENBQW1CO0FBQ2Ysd0JBQVEsZ0JBQVUsT0FBVixFQUFtQixRQUFuQixFQUE2QjtBQUNqQywyQkFBTyxPQUFQLENBQ0kscUZBQXFGLE1BQU0sR0FBTixFQUR6RixFQUVJLFVBQVUsSUFBVixFQUFnQjtBQUNaLGlDQUFTLElBQVQ7QUFDSCxxQkFKTDtBQU1IO0FBUmMsYUFBbkI7O0FBV0Esa0JBQU0sRUFBTixDQUFVLG9CQUFWLEVBQWdDLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUFBLGdDQUNyQixPQUFLLFFBRGdCO0FBQUEsb0JBQ3JDLEtBRHFDLGFBQ3JDLEtBRHFDO0FBQUEsb0JBQzlCLElBRDhCLGFBQzlCLElBRDhCOztBQUU3Qyx1QkFBSyxPQUFMLENBQWEsR0FBRyxJQUFILENBQVEsS0FBckIsRUFBNEIsS0FBNUIsRUFBbUMsSUFBbkMsRUFBeUMsVUFBQyxXQUFELEVBQWlCO0FBQ3RELDJCQUFLLE9BQUwsQ0FBYSxPQUFLLGFBQWxCLEVBQWlDLEtBQWpDLEVBQXdDLFdBQXhDO0FBQ0gsaUJBRkQ7QUFHRCxhQUxEO0FBTUQ7O0FBekd1QixLQUE1Qjs7QUE2R0E7QUFDQTtBQUNBLE1BQUUsRUFBRixDQUFNLFVBQU4sSUFBcUIsVUFBVSxPQUFWLEVBQW9CO0FBQ3JDLGVBQU8sS0FBSyxJQUFMLENBQVcsWUFBVztBQUN6QixnQkFBSyxDQUFDLEVBQUUsSUFBRixDQUFRLElBQVIsRUFBYyxZQUFZLFVBQTFCLENBQU4sRUFBK0M7QUFDM0Msa0JBQUUsSUFBRixDQUFRLElBQVIsRUFBYyxZQUNWLFVBREosRUFDZ0IsSUFBSSxNQUFKLENBQVksSUFBWixFQUFrQixPQUFsQixDQURoQjtBQUVIO0FBQ0osU0FMTSxDQUFQO0FBTUgsS0FQRDtBQVNILENBM0tBLEVBMktJLE1BM0tKLEVBMktZLE1BM0taLEVBMktvQixRQTNLcEIiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLy8gdGhlIHNlbWktY29sb24gYmVmb3JlIGZ1bmN0aW9uIGludm9jYXRpb24gaXMgYSBzYWZldHkgbmV0IGFnYWluc3QgY29uY2F0ZW5hdGVkXG4vLyBzY3JpcHRzIGFuZC9vciBvdGhlciBwbHVnaW5zIHdoaWNoIG1heSBub3QgYmUgY2xvc2VkIHByb3Blcmx5LlxuOyggZnVuY3Rpb24oICQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCApIHtcblxuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgLy8gdW5kZWZpbmVkIGlzIHVzZWQgaGVyZSBhcyB0aGUgdW5kZWZpbmVkIGdsb2JhbCB2YXJpYWJsZSBpbiBFQ01BU2NyaXB0IDMgaXNcbiAgICAvLyBtdXRhYmxlIChpZS4gaXQgY2FuIGJlIGNoYW5nZWQgYnkgc29tZW9uZSBlbHNlKS4gdW5kZWZpbmVkIGlzbid0IHJlYWxseSBiZWluZ1xuICAgIC8vIHBhc3NlZCBpbiBzbyB3ZSBjYW4gZW5zdXJlIHRoZSB2YWx1ZSBvZiBpdCBpcyB0cnVseSB1bmRlZmluZWQuIEluIEVTNSwgdW5kZWZpbmVkXG4gICAgLy8gY2FuIG5vIGxvbmdlciBiZSBtb2RpZmllZC5cblxuICAgIC8vIHdpbmRvdyBhbmQgZG9jdW1lbnQgYXJlIHBhc3NlZCB0aHJvdWdoIGFzIGxvY2FsIHZhcmlhYmxlcyByYXRoZXIgdGhhbiBnbG9iYWxcbiAgICAvLyBhcyB0aGlzIChzbGlnaHRseSkgcXVpY2tlbnMgdGhlIHJlc29sdXRpb24gcHJvY2VzcyBhbmQgY2FuIGJlIG1vcmUgZWZmaWNpZW50bHlcbiAgICAvLyBtaW5pZmllZCAoZXNwZWNpYWxseSB3aGVuIGJvdGggYXJlIHJlZ3VsYXJseSByZWZlcmVuY2VkIGluIHlvdXIgcGx1Z2luKS5cblxuICAgIC8vIENyZWF0ZSB0aGUgZGVmYXVsdHMgb25jZVxuICAgIHZhciBwbHVnaW5OYW1lID0gXCJJbGlhV2VhdGhlcldpZGdldFwiLFxuICAgICAgICBkZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIHByb3BlcnR5TmFtZTogXCJ2YWx1ZVwiLFxuICAgICAgICB9O1xuXG4gICAgLy8gVGhlIGFjdHVhbCBwbHVnaW4gY29uc3RydWN0b3JcbiAgICBmdW5jdGlvbiBQbHVnaW4gKCBlbGVtZW50LCBvcHRpb25zICkge1xuICAgICAgICB0aGlzLmVsZW1lbnQgPSBlbGVtZW50O1xuICAgICAgICAvLyBqUXVlcnkgaGFzIGFuIGV4dGVuZCBtZXRob2Qgd2hpY2ggbWVyZ2VzIHRoZSBjb250ZW50cyBvZiB0d28gb3JcbiAgICAgICAgLy8gbW9yZSBvYmplY3RzLCBzdG9yaW5nIHRoZSByZXN1bHQgaW4gdGhlIGZpcnN0IG9iamVjdC4gVGhlIGZpcnN0IG9iamVjdFxuICAgICAgICAvLyBpcyBnZW5lcmFsbHkgZW1wdHkgYXMgd2UgZG9uJ3Qgd2FudCB0byBhbHRlciB0aGUgZGVmYXVsdCBvcHRpb25zIGZvclxuICAgICAgICAvLyBmdXR1cmUgaW5zdGFuY2VzIG9mIHRoZSBwbHVnaW5cbiAgICAgICAgLy90aGlzLnNldHRpbmdzID0gJC5leHRlbmQoIHt9LCBkZWZhdWx0cywgb3B0aW9ucyApO1xuICAgICAgICB0aGlzLnNldHRpbmdzID0gJC5leHRlbmQoIHtcbiAgICAgICAgICAgICdjaXR5JzogJ2NpdHknLFxuICAgICAgICAgICAgJ3VuaXRzJyA6ICdtZXRyaWMnLFxuICAgICAgICAgICAgJ2xhbmcnOiAnZW4nXG4gICAgICAgIH0sIG9wdGlvbnMgKTtcbiAgICAgICAgdGhpcy5fZGVmYXVsdHMgPSBkZWZhdWx0cztcbiAgICAgICAgdGhpcy5fbmFtZSA9IHBsdWdpbk5hbWU7XG5cbiAgICAgICAgdGhpcy53ZWF0aGVySW1hZ2VzID0ge1xuICAgICAgICAgICAgJ0Nsb3Vkcyc6ICdodHRwczovL2RsLmRyb3Bib3h1c2VyY29udGVudC5jb20vdS84NDQyMTkzMy93ZWF0aGVySW1hZ2VzL2Nsb3Vkcy5qcGcnLFxuICAgICAgICAgICAgJ1JhaW4nOiAnaHR0cHM6Ly9kbC5kcm9wYm94dXNlcmNvbnRlbnQuY29tL3UvODQ0MjE5MzMvd2VhdGhlckltYWdlcy9yYWluLmpwZycsXG4gICAgICAgICAgICAnU25vdyc6ICdodHRwczovL2RsLmRyb3Bib3h1c2VyY29udGVudC5jb20vdS84NDQyMTkzMy93ZWF0aGVySW1hZ2VzL3Nub3cuanBnJyxcbiAgICAgICAgICAgICdNaXN0JzogJ2h0dHBzOi8vZGwuZHJvcGJveHVzZXJjb250ZW50LmNvbS91Lzg0NDIxOTMzL3dlYXRoZXJJbWFnZXMvbWlzdC5qcGcnLFxuICAgICAgICAgICAgJ0NsZWFyJzogJ2h0dHBzOi8vZGwuZHJvcGJveHVzZXJjb250ZW50LmNvbS91Lzg0NDIxOTMzL3dlYXRoZXJJbWFnZXMvY2xlYXIuanBnJ1xuICAgICAgICB9LFxuICAgICAgICB0aGlzLnVuaXRzTGlzdCA9IHtcbiAgICAgICAgICAgIG1ldHJpYzogXCJcXHUyMTAzXCIsXG4gICAgICAgICAgICBpbXBlcmlhbDogXCJcXHUyMTA5XCIsXG4gICAgICAgICAgICBkZWZhdWx0OiBcIlxcdTIxMkFcIlxuICAgICAgICB9LFxuXG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgIH1cbiAgICAvLyBBdm9pZCBQbHVnaW4ucHJvdG90eXBlIGNvbmZsaWN0c1xuICAgICQuZXh0ZW5kKCBQbHVnaW4ucHJvdG90eXBlLCB7XG5cbiAgICAgICAgaW5pdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB0aGlzLmdldERhdGEodGhpcy5zZXR0aW5ncy5jaXR5LCB0aGlzLnNldHRpbmdzLnVuaXRzLCB0aGlzLnNldHRpbmdzLmxhbmcsICh3ZWF0aGVyRGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZGlzcGxheUxheW91dCh0aGlzLmVsZW1lbnQpO1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0RGF0YSh0aGlzLndlYXRoZXJJbWFnZXMsIHRoaXMuc2V0dGluZ3MudW5pdHMsIHdlYXRoZXJEYXRhKTtcbiAgICAgICAgICAgICAgICB0aGlzLmluaXRBdXRvY29tcGxldGUoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldERhdGE6IGZ1bmN0aW9uKGNpdHksIHVuaXRzLCBsYW5nLCBjYWxsYmFjaykge1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogXCJqc29uXCIsXG4gICAgICAgICAgICAgICAgdXJsOiBcImh0dHA6Ly9hcGkub3BlbndlYXRoZXJtYXAub3JnL2RhdGEvMi41L3dlYXRoZXI/cT1cIisgY2l0eSArXCImdW5pdHM9XCIrIHVuaXRzICtcIiZsYW5nPVwiKyBsYW5nICtcIiZhcHBpZD02MjlkYmU0YmM2NGMyNmZkMTVkZTYzNzA3MmE5OTg4MlwiLFxuICAgICAgICAgICAgICAgIGFzeW5jOiB0cnVlLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IChkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNpdHk6IGNpdHksXG4gICAgICAgICAgICAgICAgICAgICAgICBjb3VudHJ5OiBkYXRhLnN5cy5jb3VudHJ5LFxuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcDogZGF0YS5tYWluLnRlbXAsXG4gICAgICAgICAgICAgICAgICAgICAgICB3ZWF0aGVyOiBkYXRhLndlYXRoZXJbMF0uZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICB3ZWF0aGVyQ29uZDogZGF0YS53ZWF0aGVyWzBdLm1haW5cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGlzcGxheUxheW91dDogZnVuY3Rpb24oZWxlbWVudCkge1xuXG4gICAgICAgICAgICAvL9CS0YvRgdGC0YDQsNC40LLQsNC10LwgRE9NINC4INCy0YHRgtCw0LLQu9GP0LXQvCDQtdCz0L4g0LIgSFRNTFxuICAgICAgICAgICAgJChlbGVtZW50KS5hZGRDbGFzcygnd3cnKTtcbiAgICAgICAgICAgICQoJzxkaXY+Jywge1xuICAgICAgICAgICAgICAgICdjbGFzcyc6ICdpbnB1dC13cmFwcGVyJyxcbiAgICAgICAgICAgICAgICBodG1sOiAkKCc8bGFiZWwvPicsIHtcbiAgICAgICAgICAgICAgICAgICAgZm9yOiAnY2l0eScsXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICdXcml0ZSB0aGUgY2l0eTogJ1xuICAgICAgICAgICAgICAgIH0pLmFkZChcbiAgICAgICAgICAgICAgICAgICAgJCgnPGlucHV0Lz4nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2NpdHknXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgfSkuYWRkKFxuICAgICAgICAgICAgICAgICQoJzxkaXY+Jyx7XG4gICAgICAgICAgICAgICAgICAgICdjbGFzcyc6ICd3dy1iYWNrZ3JvdW5kJyxcbiAgICAgICAgICAgICAgICAgICAgaHRtbDogJCgnPGRpdj4nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAnY2xhc3MnOiAnd3ctd3JhcHBlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICBodG1sOiAkKCc8ZGl2PicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY2xhc3MnOiAnd3ctb3RoZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGh0bWw6ICQoJzxwPicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3d3LW90aGVyLWFkZGl0aW9uJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLmFkZChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnPHA+Jywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3d3LW90aGVyLWxvY2F0aW9uJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLmFkZChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCc8ZGl2PicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3d3LXRlbXBlcmF0dXJlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHRtbDogJCgnPHNwYW4+Jywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3d3LXRlbXBlcmF0dXJlLXZhbHVlJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5hZGQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCc8c3Bhbj4nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3d3LXRlbXBlcmF0dXJlLXVuaXQnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKS5hcHBlbmRUbyhlbGVtZW50KTtcblxuICAgICAgICB9LFxuXG4gICAgICAgIHNldERhdGE6IGZ1bmN0aW9uKGltYWdlcywgdW5pdHMsIHdlYXRoZXJEYXRhKSB7XG4gICAgICAgICAgICBjb25zdCBpbWdVcmwgPSBpbWFnZXNbd2VhdGhlckRhdGEud2VhdGhlckNvbmRdO1xuICAgICAgICAgICAgY29uc3QgdW5pdCA9IHRoaXMudW5pdHNMaXN0W3VuaXRzXTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgJCgnLnd3LWJhY2tncm91bmQnKS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCAndXJsKFwiJysgaW1nVXJsICsnXCIpJyk7XG4gICAgICAgICAgICAkKCcud3ctb3RoZXItYWRkaXRpb24nKS50ZXh0KHdlYXRoZXJEYXRhLndlYXRoZXIpO1xuICAgICAgICAgICAgJCgnLnd3LW90aGVyLWxvY2F0aW9uJykudGV4dCh3ZWF0aGVyRGF0YS5jb3VudHJ5ICsgJywgJyArIHdlYXRoZXJEYXRhLmNpdHkpO1xuICAgICAgICAgICAgJCgnLnd3LXRlbXBlcmF0dXJlLXZhbHVlJykudGV4dCh3ZWF0aGVyRGF0YS50ZW1wLnRvRml4ZWQoKSk7XG4gICAgICAgICAgICAkKCcud3ctdGVtcGVyYXR1cmUtdW5pdCcpLnRleHQodW5pdCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5pdEF1dG9jb21wbGV0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgY29uc3QgJGNpdHkgPSAkKCcjY2l0eScpO1xuXG4gICAgICAgICAgJGNpdHkuYXV0b2NvbXBsZXRlKHtcbiAgICAgICAgICAgICAgc291cmNlOiBmdW5jdGlvbiAocmVxdWVzdCwgcmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgIGpRdWVyeS5nZXRKU09OKFxuICAgICAgICAgICAgICAgICAgICAgIFwiaHR0cDovL2dkLmdlb2J5dGVzLmNvbS9BdXRvQ29tcGxldGVDaXR5P2NhbGxiYWNrPT8mdGVtcGxhdGU9PGdlb2J5dGVzJTIwY2l0eT4mcT1cIiArICRjaXR5LnZhbCgpLFxuICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlKGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICAkY2l0eS5vbiggXCJhdXRvY29tcGxldGVzZWxlY3RcIiwgKGV2ZW50LCB1aSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyB1bml0cywgbGFuZyB9ID0gdGhpcy5zZXR0aW5ncztcbiAgICAgICAgICAgIHRoaXMuZ2V0RGF0YSh1aS5pdGVtLnZhbHVlLCB1bml0cywgbGFuZywgKHdlYXRoZXJEYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXREYXRhKHRoaXMud2VhdGhlckltYWdlcywgdW5pdHMsIHdlYXRoZXJEYXRhKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICB9KTtcblxuICAgIC8vIEEgcmVhbGx5IGxpZ2h0d2VpZ2h0IHBsdWdpbiB3cmFwcGVyIGFyb3VuZCB0aGUgY29uc3RydWN0b3IsXG4gICAgLy8gcHJldmVudGluZyBhZ2FpbnN0IG11bHRpcGxlIGluc3RhbnRpYXRpb25zXG4gICAgJC5mblsgcGx1Z2luTmFtZSBdID0gZnVuY3Rpb24oIG9wdGlvbnMgKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVhY2goIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYgKCAhJC5kYXRhKCB0aGlzLCBcInBsdWdpbl9cIiArIHBsdWdpbk5hbWUgKSApIHtcbiAgICAgICAgICAgICAgICAkLmRhdGEoIHRoaXMsIFwicGx1Z2luX1wiICtcbiAgICAgICAgICAgICAgICAgICAgcGx1Z2luTmFtZSwgbmV3IFBsdWdpbiggdGhpcywgb3B0aW9ucyApICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gKTtcbiAgICB9O1xuXG59ICkoIGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCApO1xuIl19
