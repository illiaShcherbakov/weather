// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;( function( $, window, document, undefined ) {

    "use strict";

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variables rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "IliaWeatherWidget",
        defaults = {
            propertyName: "value",
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        //this.settings = $.extend( {}, defaults, options );
        this.settings = $.extend( {
            'city': 'city',
            'units' : 'metric',
            'lang': 'en'
        }, options );
        this._defaults = defaults;
        this._name = pluginName;

        this.weatherImages = {
            'Clouds': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/clouds.jpg',
            'Rain': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/rain.jpg',
            'Snow': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/snow.jpg',
            'Mist': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/mist.jpg',
            'Clear': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/clear.jpg'
        },
        this.unitsList = {
            metric: "\u2103",
            imperial: "\u2109",
            default: "\u212A"
        },

        this.init();
    }
    // Avoid Plugin.prototype conflicts
    $.extend( Plugin.prototype, {

        init: function() {
            this.getData(this.settings.city, this.settings.units, this.settings.lang, (weatherData) => {
                this.displayLayout(this.element);
                this.setData(this.weatherImages, this.settings.units, weatherData);
                this.initAutocomplete();
            });
        },

        getData: function(city, units, lang, callback) {
            $.ajax({
                dataType: "json",
                url: "http://api.openweathermap.org/data/2.5/weather?q="+ city +"&units="+ units +"&lang="+ lang +"&appid=629dbe4bc64c26fd15de637072a99882",
                async: true,
                success: (data) => {
                    callback({
                        city: city,
                        country: data.sys.country,
                        temp: data.main.temp,
                        weather: data.weather[0].description,
                        weatherCond: data.weather[0].main
                    });
                }
            });
        },

        displayLayout: function(element) {

            //Выстраиваем DOM и вставляем его в HTML
            $(element).addClass('ww');
            $('<div>', {
                'class': 'input-wrapper',
                html: $('<label/>', {
                    for: 'city',
                    text: 'Write the city: '
                }).add(
                    $('<input/>', {
                        type: 'text',
                        id: 'city'
                    })
                )
            }).add(
                $('<div>',{
                    'class': 'ww-background',
                    html: $('<div>', {
                        'class': 'ww-wrapper',
                        html: $('<div>', {
                            'class': 'ww-other',
                            html: $('<p>', {
                                'class': 'ww-other-addition'
                            }).add(
                                $('<p>', {
                                    'class': 'ww-other-location'
                                })
                            )
                        }).add(
                            $('<div>', {
                                'class': 'ww-temperature',
                                html: $('<span>', {
                                    'class': 'ww-temperature-value'
                                }).add(
                                    $('<span>', {
                                        'class': 'ww-temperature-unit'
                                    })
                                )
                            })
                        )
                    })
                })
            ).appendTo(element);

        },

        setData: function(images, units, weatherData) {
            const imgUrl = images[weatherData.weatherCond];
            const unit = this.unitsList[units];
            
            $('.ww-background').css('background-image', 'url("'+ imgUrl +'")');
            $('.ww-other-addition').text(weatherData.weather);
            $('.ww-other-location').text(weatherData.country + ', ' + weatherData.city);
            $('.ww-temperature-value').text(weatherData.temp.toFixed());
            $('.ww-temperature-unit').text(unit);
        },

        initAutocomplete: function() {
          const $city = $('#city');

          $city.autocomplete({
              source: function (request, response) {
                  jQuery.getJSON(
                      "http://gd.geobytes.com/AutoCompleteCity?callback=?&template=<geobytes%20city>&q=" + $city.val(),
                      function (data) {
                          response(data);
                      }
                  );
              },
          });

          $city.on( "autocompleteselect", (event, ui) => {
            const { units, lang } = this.settings;
            this.getData(ui.item.value, units, lang, (weatherData) => {
                this.setData(this.weatherImages, units, weatherData);
            });
          });
        }

    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function( options ) {
        return this.each( function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" +
                    pluginName, new Plugin( this, options ) );
            }
        } );
    };

} )( jQuery, window, document );
